const wordsArray = require('../index.js')
console.log(JSON.stringify(wordsArray("Please could you stop the noise, I'm trying to get some rest.")))
// Returns ["Please","could","you","stop","the","noise,","I'm","trying","to","get","some","rest."]
console.log(JSON.stringify(wordsArray("そこで彼らは，オリーブ山と呼ばれる山からエルサレムに帰った。")))
// Returns ["そ","こ","で","彼","ら","は，","オ","リ","ー","ブ","山","と","呼","ば","れ","る","山","か","ら","エ","ル","サ","レ","ム","に","帰","っ","た。"]
