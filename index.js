//
// ## Returns array of words in text
// ## For CJK languages almost every char is a word,
// ## for other languages words are separated by spaces
//
module.exports = function(text) {

	// Test for CJK characters
	if (/[\u3400-\u9FBF]/.test(text)) {

		// Contains CJK characters
		var words = []
		const characters = text.split("");
		for (var i = 0; i <= characters.length - 1; i++)
			if (!containsPunctations(characters[i + 1])) {
				// Next character is "normal"
				words.push(characters[i])
			} else {
				// Next character isn't a single word
				words.push(characters[i] + characters[i + 1])
				i++;
			}

		return words

	} else {

		// Other language
		// Converts returns in spaces, removes double spaces
		text = text.replace(/(\r\n|\n|\r)/gm," ").replace(/\s+/g," ")
		// Simply split by spaces
		return text.split(" ")

	}

}

//
// ## Returns true if text contains puntaction characters
//
containsPunctations = function(text) {
	// Test string against regexp for many punctactions characters, including CJK ones
	return /[\uFF01-\uFF07,\u0021,\u003F,\u002E,\u002C,\u003A,\u003B,\uFF1A-\uFF1F,\u3002,\uFF0C-\uFF0E,\u2000-\u206F,\uFFED-\uFFEF,\u0028,\u0029]/.test(text)
}
