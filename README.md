# words-array

Converts a string into an array of words. Its supports [CJK](https://en.wikipedia.org/wiki/CJK_characters) strings.

Initialy developed for [ISBOBackend](https://gitlab.com/davideblasutto/ISBOBackend/).

## Usage
This module exports a single function.

### Example
```javascript
const wordsArray = require('words-array')
console.log(JSON.stringify(wordsArray("Please could you stop the noise, I'm trying to get some rest.")))
// Returns ["Please","could","you","stop","the","noise,","I'm","trying","to","get","some","rest."]
require('fs').writeFileSync('asd.txt',JSON.stringify(wordsArray("そこで彼らは，オリーブ山と呼ばれる山からエルサレムに帰った。")))
// Returns ["そ","こ","で","彼","ら","は，","オ","リ","ー",...,"レ","ム","に","帰","っ","た。"]
```
